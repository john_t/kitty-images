use std::{
    io,
    num::{NonZeroU128, NonZeroU32},
    process,
};

use kitty_image::{
    Action, ActionPut, ActionTransmission, Command, Format, Medium, Quietness, WrappedCommand, ID,
    UNICODE_DIACRITICS,
};

fn main() -> io::Result<()> {
    put();
    display();
    Ok(())
}

fn put() {
    let action = Action::TransmitAndDisplay(
        ActionTransmission {
            format: Format::Png,
            medium: Medium::File,
            width: 387,
            height: 480,
            ..Default::default()
        },
        ActionPut {
            unicode_placeholder: true,
            columns: 28,
            rows: 18,

            ..Default::default()
        },
    );
    let mut command = Command::with_payload_from_path(
        action,
        "/home/johnh/projects/programs/03_lib/kitty_image/examples/the_scream.png".as_ref(),
    );
    command.quietness = Quietness::SuppressAll;
    command.id = Some(ID(NonZeroU32::new(1).unwrap()));

    let mut command = WrappedCommand::new(command);
    command.double_escape = true;
    println!("\x1bPtmux;{command}\x1b\\");
}

fn display() {
    print!("\x1b[38;5;1m");
    for y in UNICODE_DIACRITICS.iter().take(18) {
        for x in UNICODE_DIACRITICS.iter().take(28) {
            print!("\u{10EEEE}{}{}", y, x);
        }
        println!();
    }
    println!("\x1b[39m");
}
