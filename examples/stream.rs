use image::io::Reader as ImageReader;
use kitty_image::{Action, ActionTransmission, Command, Format, Medium, WrappedCommand};
use std::io::stdout;

fn main() -> Result<(), image::error::ImageError> {
    let action = Action::TransmitAndDisplay(
        ActionTransmission {
            format: Format::Png,
            medium: Medium::Direct,
            width: 367,
            height: 480,
            ..Default::default()
        },
        kitty_image::ActionPut {
            move_cursor: true,
            ..Default::default()
        },
    );

    let mut command = Command::new(action);
    command.payload = include_bytes!("the_scream.png").to_vec().into();
    let command = WrappedCommand::new(command);
    command.send_chunked(&mut stdout())?;

    Ok(())
}
