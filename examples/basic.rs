use std::default;

use kitty_image::{Action, ActionTransmission, Command, Format, Medium, WrappedCommand};

fn main() {
    let action = Action::TransmitAndDisplay(
        ActionTransmission {
            format: Format::Png,
            medium: Medium::File,
            width: 367,
            height: 480,
            ..Default::default()
        },
        kitty_image::ActionPut::default(),
    );

    let command = Command::with_payload_from_path(action, "examples/the_scream.png".as_ref());
    let command = WrappedCommand::new(command);
    println!("{command}");
}
