# Kitty Image

A simple rust crate to allow you to display images using
the [Kitty Image Protocol](https://github.com/kovidgoyal/kitty), most likely
for the Kitty Terminal emulator.

Observe the ease in which you can display an image located on the file system:

```rust
// Create an action
let action = Action::TransmitAndDisplay(
    ActionTransmission {
        format: Format::Png,
        medium: Medium::File,
        width: 367,
        height: 480,
        ..Default::default()
    },
    kitty_image::ActionPut::default(),
);

// Add the payload to the command
let command = Command::with_payload_from_path(action, "examples/the_scream.png".as_ref());

// Wrap the command in escape codes
let command = WrappedCommand::new(command);

// Print it
println!("{command}");
```

Check out the [examples](https://gitlab.com/john_t/kitty-images/-/tree/master/examples?ref_type=heads)
