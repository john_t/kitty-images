/// For unicode placeholders.
///
/// Read [this](https://sw.kovidgoyal.net/kitty/graphics-protocol/#unicode-placeholders)
/// and look at [this](https://gitlab.com/john_t/kitty-images/-/blob/main/examples/tmux.rs) example for usage.
pub const UNICODE_DIACRITICS: &[char] = &[
    '\u{0305}',  // Combining Overline
    '\u{030D}',  // Combining Vertical Line Above
    '\u{030E}',  // Combining Double Vertical Line Above
    '\u{0310}',  // Combining Candrabindu
    '\u{0312}',  // Combining Turned Comma Above
    '\u{033D}',  // Combining X Above
    '\u{033E}',  // Combining Vertical Tilde
    '\u{033F}',  // Combining Double Overline
    '\u{0346}',  // Combining Bridge Above
    '\u{034A}',  // Combining Not Tilde Above
    '\u{034B}',  // Combining Homothetic Above
    '\u{034C}',  // Combining Almost Equal To Above
    '\u{0350}',  // Combining Right Arrowhead Above
    '\u{0351}',  // Combining Left Half Ring Above
    '\u{0352}',  // Combining Fermata
    '\u{0357}',  // Combining Right Half Ring Above
    '\u{035B}',  // Combining Zigzag Above
    '\u{0363}',  // Combining Latin Small Letter A
    '\u{0364}',  // Combining Latin Small Letter E
    '\u{0365}',  // Combining Latin Small Letter I
    '\u{0366}',  // Combining Latin Small Letter O
    '\u{0367}',  // Combining Latin Small Letter U
    '\u{0368}',  // Combining Latin Small Letter C
    '\u{0369}',  // Combining Latin Small Letter D
    '\u{036A}',  // Combining Latin Small Letter H
    '\u{036B}',  // Combining Latin Small Letter M
    '\u{036C}',  // Combining Latin Small Letter R
    '\u{036D}',  // Combining Latin Small Letter T
    '\u{036E}',  // Combining Latin Small Letter V
    '\u{036F}',  // Combining Latin Small Letter X
    '\u{0483}',  // Combining Cyrillic Titlo
    '\u{0484}',  // Combining Cyrillic Palatalization
    '\u{0485}',  // Combining Cyrillic Dasia Pneumata
    '\u{0486}',  // Combining Cyrillic Psili Pneumata
    '\u{0487}',  // Combining Cyrillic Pokrytie
    '\u{0592}',  // Hebrew Accent Segol
    '\u{0593}',  // Hebrew Accent Shalshelet
    '\u{0594}',  // Hebrew Accent Zaqef Qatan
    '\u{0595}',  // Hebrew Accent Zaqef Gadol
    '\u{0597}',  // Hebrew Accent Revia
    '\u{0598}',  // Hebrew Accent Zarqa
    '\u{0599}',  // Hebrew Accent Pashta
    '\u{059C}',  // Hebrew Accent Geresh
    '\u{059D}',  // Hebrew Accent Geresh Muqdam
    '\u{059E}',  // Hebrew Accent Gershayim
    '\u{059F}',  // Hebrew Accent Qarney Para
    '\u{05A0}',  // Hebrew Accent Telisha Gedola
    '\u{05A1}',  // Hebrew Accent Pazer
    '\u{05A8}',  // Hebrew Accent Qadma
    '\u{05A9}',  // Hebrew Accent Telisha Qetana
    '\u{05AB}',  // Hebrew Accent Ole
    '\u{05AC}',  // Hebrew Accent Iluy
    '\u{05AF}',  // Hebrew Mark Masora Circle
    '\u{05C4}',  // Hebrew Mark Upper Dot
    '\u{0610}',  // Arabic Sign Sallallahou Alayhe Wassallam
    '\u{0611}',  // Arabic Sign Alayhe Assallam
    '\u{0612}',  // Arabic Sign Rahmatullah Alayhe
    '\u{0613}',  // Arabic Sign Radi Allahou Anhu
    '\u{0614}',  // Arabic Sign Takhallus
    '\u{0615}',  // Arabic Small High Tah
    '\u{0616}',  // Arabic Small High Ligature Alef With Lam With Yeh
    '\u{0617}',  // Arabic Small High Zain
    '\u{0657}',  // Arabic Inverted Damma
    '\u{0658}',  // Arabic Mark Noon Ghunna
    '\u{0659}',  // Arabic Zwarakay
    '\u{065A}',  // Arabic Vowel Sign Small V Above
    '\u{065B}',  // Arabic Vowel Sign Inverted Small V Above
    '\u{065D}',  // Arabic Reversed Damma
    '\u{065E}',  // Arabic Fatha With Two Dots
    '\u{06D6}',  // Arabic Small High Ligature Sad With Lam With Alef Maksura
    '\u{06D7}',  // Arabic Small High Ligature Qaf With Lam With Alef Maksura
    '\u{06D8}',  // Arabic Small High Meem Initial Form
    '\u{06D9}',  // Arabic Small High Lam Alef
    '\u{06DA}',  // Arabic Small High Jeem
    '\u{06DB}',  // Arabic Small High Three Dots
    '\u{06DC}',  // Arabic Small High Seen
    '\u{06DF}',  // Arabic Small High Rounded Zero
    '\u{06E0}',  // Arabic Small High Upright Rectangular Zero
    '\u{06E1}',  // Arabic Small High Dotless Head Of Khah
    '\u{06E2}',  // Arabic Small High Meem Isolated Form
    '\u{06E4}',  // Arabic Small High Madda
    '\u{06E7}',  // Arabic Small High Yeh
    '\u{06E8}',  // Arabic Small High Noon
    '\u{06EB}',  // Arabic Empty Centre High Stop
    '\u{06EC}',  // Arabic Rounded High Stop With Filled Centre
    '\u{0730}',  // Syriac Pthaha Above
    '\u{0732}',  // Syriac Pthaha Dotted
    '\u{0733}',  // Syriac Zqapha Above
    '\u{0735}',  // Syriac Zqapha Dotted
    '\u{0736}',  // Syriac Rbasa Above
    '\u{073A}',  // Syriac Hbasa Above
    '\u{073D}',  // Syriac Esasa Above
    '\u{073F}',  // Syriac Rwaha
    '\u{0740}',  // Syriac Feminine Dot
    '\u{0741}',  // Syriac Qushshaya
    '\u{0743}',  // Syriac Two Vertical Dots Above
    '\u{0745}',  // Syriac Three Dots Above
    '\u{0747}',  // Syriac Oblique Line Above
    '\u{0749}',  // Syriac Music
    '\u{074A}',  // Syriac Barrekh
    '\u{07EB}',  // Nko Combining Short High Tone
    '\u{07EC}',  // Nko Combining Short Low Tone
    '\u{07ED}',  // Nko Combining Short Rising Tone
    '\u{07EE}',  // Nko Combining Long Descending Tone
    '\u{07EF}',  // Nko Combining Long High Tone
    '\u{07F0}',  // Nko Combining Long Low Tone
    '\u{07F1}',  // Nko Combining Long Rising Tone
    '\u{07F3}',  // Nko Combining Double Dot Above
    '\u{0816}',  // Samaritan Mark In
    '\u{0817}',  // Samaritan Mark In-alaf
    '\u{0818}',  // Samaritan Mark Occlusion
    '\u{0819}',  // Samaritan Mark Dagesh
    '\u{081B}',  // Samaritan Mark Epenthetic Yut
    '\u{081C}',  // Samaritan Vowel Sign Long E
    '\u{081D}',  // Samaritan Vowel Sign E
    '\u{081E}',  // Samaritan Vowel Sign Overlong Aa
    '\u{081F}',  // Samaritan Vowel Sign Long Aa
    '\u{0820}',  // Samaritan Vowel Sign Aa
    '\u{0821}',  // Samaritan Vowel Sign Overlong A
    '\u{0822}',  // Samaritan Vowel Sign Long A
    '\u{0823}',  // Samaritan Vowel Sign A
    '\u{0825}',  // Samaritan Vowel Sign Short A
    '\u{0826}',  // Samaritan Vowel Sign Long U
    '\u{0827}',  // Samaritan Vowel Sign U
    '\u{0829}',  // Samaritan Vowel Sign Long I
    '\u{082A}',  // Samaritan Vowel Sign I
    '\u{082B}',  // Samaritan Vowel Sign O
    '\u{082C}',  // Samaritan Vowel Sign Sukun
    '\u{082D}',  // Samaritan Mark Nequdaa
    '\u{0951}',  // Devanagari Stress Sign Udatta
    '\u{0953}',  // Devanagari Grave Accent
    '\u{0954}',  // Devanagari Acute Accent
    '\u{0F82}',  // Tibetan Sign Nyi Zla Naa Da
    '\u{0F83}',  // Tibetan Sign Sna Ldan
    '\u{0F86}',  // Tibetan Sign Lci Rtags
    '\u{0F87}',  // Tibetan Sign Yang Rtags
    '\u{135D}',  // Ethiopic Combining Gemination And Vowel Length Mark
    '\u{135E}',  // Ethiopic Combining Vowel Length Mark
    '\u{135F}',  // Ethiopic Combining Gemination Mark
    '\u{17DD}',  // Khmer Sign Atthacan
    '\u{193A}',  // Limbu Sign Kemphreng
    '\u{1A17}',  // Buginese Vowel Sign I
    '\u{1A75}',  // Tai Tham Sign Tone-1
    '\u{1A76}',  // Tai Tham Sign Tone-2
    '\u{1A77}',  // Tai Tham Sign Khuen Tone-3
    '\u{1A78}',  // Tai Tham Sign Khuen Tone-4
    '\u{1A79}',  // Tai Tham Sign Khuen Tone-5
    '\u{1A7A}',  // Tai Tham Sign Ra Haam
    '\u{1A7B}',  // Tai Tham Sign Mai Sam
    '\u{1A7C}',  // Tai Tham Sign Khuen-lue Karan
    '\u{1B6B}',  // Balinese Musical Symbol Combining Tegeh
    '\u{1B6D}',  // Balinese Musical Symbol Combining Kempul
    '\u{1B6E}',  // Balinese Musical Symbol Combining Kempli
    '\u{1B6F}',  // Balinese Musical Symbol Combining Jegogan
    '\u{1B70}',  // Balinese Musical Symbol Combining Kempul With Jegogan
    '\u{1B71}',  // Balinese Musical Symbol Combining Kempli With Jegogan
    '\u{1B72}',  // Balinese Musical Symbol Combining Bende
    '\u{1B73}',  // Balinese Musical Symbol Combining Gong
    '\u{1CD0}',  // Vedic Tone Karshana
    '\u{1CD1}',  // Vedic Tone Shara
    '\u{1CD2}',  // Vedic Tone Prenkha
    '\u{1CDA}',  // Vedic Tone Double Svarita
    '\u{1CDB}',  // Vedic Tone Triple Svarita
    '\u{1CE0}',  // Vedic Tone Rigvedic Kashmiri Independent Svarita
    '\u{1DC0}',  // Combining Dotted Grave Accent
    '\u{1DC1}',  // Combining Dotted Acute Accent
    '\u{1DC3}',  // Combining Suspension Mark
    '\u{1DC4}',  // Combining Macron-acute
    '\u{1DC5}',  // Combining Grave-macron
    '\u{1DC6}',  // Combining Macron-grave
    '\u{1DC7}',  // Combining Acute-macron
    '\u{1DC8}',  // Combining Grave-acute-grave
    '\u{1DC9}',  // Combining Acute-grave-acute
    '\u{1DCB}',  // Combining Breve-macron
    '\u{1DCC}',  // Combining Macron-breve
    '\u{1DD1}',  // Combining Ur Above
    '\u{1DD2}',  // Combining Us Above
    '\u{1DD3}',  // Combining Latin Small Letter Flattened Open A Above
    '\u{1DD4}',  // Combining Latin Small Letter Ae
    '\u{1DD5}',  // Combining Latin Small Letter Ao
    '\u{1DD6}',  // Combining Latin Small Letter Av
    '\u{1DD7}',  // Combining Latin Small Letter C Cedilla
    '\u{1DD8}',  // Combining Latin Small Letter Insular D
    '\u{1DD9}',  // Combining Latin Small Letter Eth
    '\u{1DDA}',  // Combining Latin Small Letter G
    '\u{1DDB}',  // Combining Latin Letter Small Capital G
    '\u{1DDC}',  // Combining Latin Small Letter K
    '\u{1DDD}',  // Combining Latin Small Letter L
    '\u{1DDE}',  // Combining Latin Letter Small Capital L
    '\u{1DDF}',  // Combining Latin Letter Small Capital M
    '\u{1DE0}',  // Combining Latin Small Letter N
    '\u{1DE1}',  // Combining Latin Letter Small Capital N
    '\u{1DE2}',  // Combining Latin Letter Small Capital R
    '\u{1DE3}',  // Combining Latin Small Letter R Rotunda
    '\u{1DE4}',  // Combining Latin Small Letter S
    '\u{1DE5}',  // Combining Latin Small Letter Long S
    '\u{1DE6}',  // Combining Latin Small Letter Z
    '\u{1DFE}',  // Combining Left Arrowhead Above
    '\u{20D0}',  // Combining Left Harpoon Above
    '\u{20D1}',  // Combining Right Harpoon Above
    '\u{20D4}',  // Combining Anticlockwise Arrow Above
    '\u{20D5}',  // Combining Clockwise Arrow Above
    '\u{20D6}',  // Combining Left Arrow Above
    '\u{20D7}',  // Combining Right Arrow Above
    '\u{20DB}',  // Combining Three Dots Above
    '\u{20DC}',  // Combining Four Dots Above
    '\u{20E1}',  // Combining Left Right Arrow Above
    '\u{20E7}',  // Combining Annuity Symbol
    '\u{20E9}',  // Combining Wide Bridge Above
    '\u{20F0}',  // Combining Asterisk Above
    '\u{2CEF}',  // Coptic Combining Ni Above
    '\u{2CF0}',  // Coptic Combining Spiritus Asper
    '\u{2CF1}',  // Coptic Combining Spiritus Lenis
    '\u{2DE0}',  // Combining Cyrillic Letter Be
    '\u{2DE1}',  // Combining Cyrillic Letter Ve
    '\u{2DE2}',  // Combining Cyrillic Letter Ghe
    '\u{2DE3}',  // Combining Cyrillic Letter De
    '\u{2DE4}',  // Combining Cyrillic Letter Zhe
    '\u{2DE5}',  // Combining Cyrillic Letter Ze
    '\u{2DE6}',  // Combining Cyrillic Letter Ka
    '\u{2DE7}',  // Combining Cyrillic Letter El
    '\u{2DE8}',  // Combining Cyrillic Letter Em
    '\u{2DE9}',  // Combining Cyrillic Letter En
    '\u{2DEA}',  // Combining Cyrillic Letter O
    '\u{2DEB}',  // Combining Cyrillic Letter Pe
    '\u{2DEC}',  // Combining Cyrillic Letter Er
    '\u{2DED}',  // Combining Cyrillic Letter Es
    '\u{2DEE}',  // Combining Cyrillic Letter Te
    '\u{2DEF}',  // Combining Cyrillic Letter Ha
    '\u{2DF0}',  // Combining Cyrillic Letter Tse
    '\u{2DF1}',  // Combining Cyrillic Letter Che
    '\u{2DF2}',  // Combining Cyrillic Letter Sha
    '\u{2DF3}',  // Combining Cyrillic Letter Shcha
    '\u{2DF4}',  // Combining Cyrillic Letter Fita
    '\u{2DF5}',  // Combining Cyrillic Letter Es-te
    '\u{2DF6}',  // Combining Cyrillic Letter A
    '\u{2DF7}',  // Combining Cyrillic Letter Ie
    '\u{2DF8}',  // Combining Cyrillic Letter Djerv
    '\u{2DF9}',  // Combining Cyrillic Letter Monograph Uk
    '\u{2DFA}',  // Combining Cyrillic Letter Yat
    '\u{2DFB}',  // Combining Cyrillic Letter Yu
    '\u{2DFC}',  // Combining Cyrillic Letter Iotified A
    '\u{2DFD}',  // Combining Cyrillic Letter Little Yus
    '\u{2DFE}',  // Combining Cyrillic Letter Big Yus
    '\u{2DFF}',  // Combining Cyrillic Letter Iotified Big Yus
    '\u{A66F}',  // Combining Cyrillic Vzmet
    '\u{A67C}',  // Combining Cyrillic Kavyka
    '\u{A67D}',  // Combining Cyrillic Payerok
    '\u{A6F0}',  // Bamum Combining Mark Koqndon
    '\u{A6F1}',  // Bamum Combining Mark Tukwentis
    '\u{A8E0}',  // Combining Devanagari Digit Zero
    '\u{A8E1}',  // Combining Devanagari Digit One
    '\u{A8E2}',  // Combining Devanagari Digit Two
    '\u{A8E3}',  // Combining Devanagari Digit Three
    '\u{A8E4}',  // Combining Devanagari Digit Four
    '\u{A8E5}',  // Combining Devanagari Digit Five
    '\u{A8E6}',  // Combining Devanagari Digit Six
    '\u{A8E7}',  // Combining Devanagari Digit Seven
    '\u{A8E8}',  // Combining Devanagari Digit Eight
    '\u{A8E9}',  // Combining Devanagari Digit Nine
    '\u{A8EA}',  // Combining Devanagari Letter A
    '\u{A8EB}',  // Combining Devanagari Letter U
    '\u{A8EC}',  // Combining Devanagari Letter Ka
    '\u{A8ED}',  // Combining Devanagari Letter Na
    '\u{A8EE}',  // Combining Devanagari Letter Pa
    '\u{A8EF}',  // Combining Devanagari Letter Ra
    '\u{A8F0}',  // Combining Devanagari Letter Vi
    '\u{A8F1}',  // Combining Devanagari Sign Avagraha
    '\u{AAB0}',  // Tai Viet Mai Kang
    '\u{AAB2}',  // Tai Viet Vowel I
    '\u{AAB3}',  // Tai Viet Vowel Ue
    '\u{AAB7}',  // Tai Viet Mai Khit
    '\u{AAB8}',  // Tai Viet Vowel Ia
    '\u{AABE}',  // Tai Viet Vowel Am
    '\u{AABF}',  // Tai Viet Tone Mai Ek
    '\u{AAC1}',  // Tai Viet Tone Mai Tho
    '\u{FE20}',  // Combining Ligature Left Half
    '\u{FE21}',  // Combining Ligature Right Half
    '\u{FE22}',  // Combining Double Tilde Left Half
    '\u{FE23}',  // Combining Double Tilde Right Half
    '\u{FE24}',  // Combining Macron Left Half
    '\u{FE25}',  // Combining Macron Right Half
    '\u{FE26}',  // Combining Conjoining Macron
    '\u{10A0F}', // Kharoshthi Sign Visarga
    '\u{10A38}', // Kharoshthi Sign Bar Above
    '\u{1D185}', // Musical Symbol Combining Doit
    '\u{1D186}', // Musical Symbol Combining Rip
    '\u{1D187}', // Musical Symbol Combining Flip
    '\u{1D188}', // Musical Symbol Combining Smear
    '\u{1D189}', // Musical Symbol Combining Bend
    '\u{1D1AA}', // Musical Symbol Combining Down Bow
    '\u{1D1AB}', // Musical Symbol Combining Up Bow
    '\u{1D1AC}', // Musical Symbol Combining Harmonic
    '\u{1D1AD}', // Musical Symbol Combining Snap Pizzicato
    '\u{1D242}', // Combining Greek Musical Triseme
    '\u{1D243}', // Combining Greek Musical Tetraseme
    '\u{1D244}', // Combining Greek Musical Pentaseme
];
